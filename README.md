# chrome_stock_unusual

## 介绍

        这是一个股票异动监听浏览器插件

## 软件架构

软件架构说明

## 安装教程

1. xxxx
2. xxxx
3. xxxx

## 使用说明

### **插件界面**

![shot1.png](https://gitee.com/KnightKun/chrome_stock_unusual/raw/develop/screenshot/shot1.png)

### **选择界面**

![](https://gitee.com/KnightKun/chrome_stock_unusual/raw/develop/screenshot/select.png)

### **设置界面**

![Document.png](https://gitee.com/KnightKun/chrome_stock_unusual/raw/develop/screenshot/setting.png)

#### **设置界面板块说明**

**所有**         对应tab中的所有异动:同步自东方财富的实时异动数据

**订阅**         对应tab中的订阅异动

                         选项:继承所有中的类型筛选

                                 **是**        筛选异动类型是跟**所有**中的筛选类型一样

                                 **否**        订阅所有以带动类型

**详情**        选择查看跳转的网站

**关注**         对应tab中的关注:针对特定股票的筛选
