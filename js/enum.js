// 移动类型
var positiveClassify = [ 8201 , 8202 , 8193 , 4 , 32 , 64 , 8207 , 8209 , 8211 , 8213 , 8215 ]
    negativeClassify=[ 8204 , 8203 , 8194 , 8 , 16 , 128 , 8208 , 8210 , 8212 , 8214 , 8216 ]
var tClassify = {
    8201:"火箭发射",
    8202:"快速反弹",
    8193:"大笔买入 ",
    4:"封涨停板",
    32:"打开跌停板",
    64:"有大买盘",
    8207:"竞价上涨",
    8209:"高开5日线",
    8211:"向上缺口",
    8213:"60新高",
    8215:"60日大幅上涨",

    8204:"加速下跌",
    8203:"高台跳水",
    8194:"大笔卖出 ",
    8:"封跌停板",
    16:"打开涨停板",
    128:"有大卖盘",
    8208:"竞价下跌",
    8210:"低开5日线",
    8212:"向下缺口",
    8214:"60新低",
    8216:"60日大幅下跌",
}


var defaultFocusType = 1;
var focusTypeEnum={
    1:"owener",
    2:"follow"
}
var focusTypeValue={
    1:"持仓",
    2:"自选"
}

var upColor = "red", downColor="green";
var classifyColor={
    8201:upColor,
    8202:upColor,
    8193:upColor,
    4:upColor,
    32:upColor,
    64:upColor,
    8207:upColor,
    8209:upColor,
    8211:upColor,
    8213:upColor,
    8215:upColor,

    8204:downColor,
    8203:downColor,
    8194:downColor,
    8:downColor,
    16:downColor,
    128:downColor,
    8208:downColor,
    8210:downColor,
    8212:downColor,
    8214:downColor,
    8216:downColor,
}